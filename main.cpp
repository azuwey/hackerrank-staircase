/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/20/2019
 * Source: HackerRank - https://www.hackerrank.com/challenges/staircase/problem
 **/
#include <bits/stdc++.h>

using namespace std;

void staircase(int n) {
  for (int i = 0; i < n; ++i) {
    for (int k = n; k > 0; --k) cout << ((i + 2 > k) ? "#" : " ");
    cout << "\n";
  }
}

int main() {
  int n;
  cin >> n;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');

  staircase(n);

  return 0;
}
