# Staircase

## Problem

Consider a staircase of size `n = 4`

```s
   #
  ##
 ###
####
```

Observe that its base and height are both equal to `n`, and the image is drawn using # symbols and spaces. The last line is not preceded by any spaces.

### Function Description

It should print a staircase as described above.

`staircase` has the following parameter(s):

- `n`: an integer

### Input Format

A single integer, `n`, denoting the size of the staircase.

### Constraints

- 0 <= `n` <= 100

### Output Format

Print a staircase of size `n` using # symbols and spaces.

Note: The last line must have 0 spaces in it.

#### Sample Input

```s
6
```

#### Sample Output

```s
     #
    ##
   ###
  ####
 #####
######
```

#### Explanation

The staircase is right-aligned, composed of # symbols and spaces, and has a height and width of n = 6

## Source

[HackerRank - Staircase](https://www.hackerrank.com/challenges/staircase/problem)